﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace AIGame
{
    public class NPC : Entity
    {
        public Entity Target { get; set; }
        public Color Color { get; set; }

        private const int Width = 48;
        private const int Height = 48;
        private Vector2 TopLeftOffset { get; set; }

        public NPC(Entity target)
        {
            Target = target;
            Color = Color.Green;
            TopLeftOffset = new Vector2(Width / 2, Height / 2);
        }

        public override void Initialize()
        {
            var viewport = Game.Graphics.GraphicsDevice.Viewport;
            Position = new Vector2((float)viewport.Width * 3 / 4, (float)viewport.Height / 2);
        }

        public override void LoadContent()
        {
            Texture = new Texture2D(Game.GraphicsDevice, 1, 1);
            Texture.SetData(new[] { Color.White });
        }

        public override void Update(float deltaSeconds)
        {
            ClampPositionToViewport();
        }

        public override void Draw(float deltaSeconds)
        {
            var topLeft = Position - TopLeftOffset;
            var rect = new Rectangle((int)topLeft.X, (int)topLeft.Y, Width, Height);

            Game.SpriteBatch.Draw(Texture, rect, Color);
        }
    }
}
