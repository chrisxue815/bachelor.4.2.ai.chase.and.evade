using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class AIGame : Game
    {
        public static AIGame Instance { get; private set; }

        public GraphicsDeviceManager Graphics { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        private List<Entity> Entities { get; set; }
        private Player Player { get; set; }
        private NPC NPC { get; set; }
        private FiniteStateMachine FiniteStateMachine { get; set; }

        public AIGame()
        {
            Instance = this;

            Content.RootDirectory = "Content";

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720
            };
        }

        protected override void Initialize()
        {
            Player = new Player();
            NPC = new NPC(Player);
            FiniteStateMachine = new FiniteStateMachine(NPC);

            Entities = new List<Entity>
            {
                Player,
                NPC,
                FiniteStateMachine
            };

            foreach (var entity in Entities)
            {
                entity.Initialize();
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // create a SpriteBatch, and load the textures and font that we'll need
            // during the game.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (var entity in Entities)
            {
                entity.LoadContent();
            }

            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            // Check for exit.
            var keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Escape)) Exit();

            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            foreach (var entity in Entities)
            {
                entity.Update(deltaSeconds);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin();

            foreach (var entity in Entities)
            {
                entity.Draw(deltaSeconds);
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }

    static class Program
    {
        static void Main()
        {
            using (var game = new AIGame())
            {
                game.Run();
            }
        }
    }
}
