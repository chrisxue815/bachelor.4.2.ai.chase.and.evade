﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AIGame
{
    public enum State { Idle, Walking, Chasing, Searching };
    public enum Operation { None, Any, Chase, Search, StopSearching };

    public class FiniteStateMachine : Entity
    {
        private const float Speed = 150;
        private const float DetectionRadius = 200;
        private const float SearchingTotalTimeout = 10;

        private Dictionary<State, Dictionary<Operation, Action>> Rules { get; set; }
        private Operation Operation { get; set; }

        private NPC NPC { get; set; }

        private float DeltaSeconds { get; set; }
        private bool WalkingLeft { get; set; }
        private float SearchingTimeout { get; set; }
        private Vector2 TargetLastPosition { get; set; }

        private SpriteFont Font { get; set; }

        private State _state;
        private State State
        {
            get { return _state; }
            set
            {
                _state = value;
                switch (_state)
                {
                    case State.Walking:
                        NPC.Color = Color.Blue;
                        break;
                    case State.Chasing:
                        NPC.Color = Color.Pink;
                        break;
                    case State.Searching:
                        NPC.Color = Color.Yellow;
                        TargetLastPosition = NPC.Target.Position;
                        SearchingTimeout = SearchingTotalTimeout;
                        break;
                }
            }
        }

        public FiniteStateMachine(NPC npc)
        {
            NPC = npc;
            State = State.Idle;
            WalkingLeft = true;

            Rules = new Dictionary<State, Dictionary<Operation, Action>>();
            foreach (State e in Enum.GetValues(typeof(State)))
            {
                Rules[e] = new Dictionary<Operation, Action>();
            }

            InitOperationRule();
        }

        private void InitOperationRule()
        {
            Rules[State.Idle][Operation.Any] = () =>
            {
                State = State.Walking;
            };

            Rules[State.Walking][Operation.Chase] = () =>
            {
                State = State.Chasing;
            };

            Rules[State.Chasing][Operation.Search] = () =>
            {
                State = State.Searching;
            };

            Rules[State.Searching][Operation.Chase] = () =>
            {
                State = State.Chasing;
            };

            Rules[State.Searching][Operation.StopSearching] = () =>
            {
                State = State.Walking;
            };

            Rules[State.Walking][Operation.Any] = () =>
            {
                var movement = WalkingLeft ? -1 : 1;
                var x = NPC.Position.X + movement * Speed * DeltaSeconds;
                NPC.Position = new Vector2(x, NPC.Position.Y);

                // update direction
                var viewport = Game.Graphics.GraphicsDevice.Viewport;
                if (NPC.Position.X < viewport.X) WalkingLeft = false;
                else if (NPC.Position.X > viewport.X + viewport.Width) WalkingLeft = true;

                var distance = Vector2.Distance(NPC.Position, NPC.Target.Position);
                if (distance < DetectionRadius) Operation = Operation.Chase;
            };

            Rules[State.Chasing][Operation.Any] = () =>
            {
                var npcToTarget = NPC.Target.Position - NPC.Position;
                var look = Vector2.Normalize(npcToTarget);
                NPC.Position += Speed * DeltaSeconds * look;

                var distance = npcToTarget.Length();
                if (distance >= DetectionRadius) Operation = Operation.Search;
            };

            Rules[State.Searching][Operation.Any] = () =>
            {
                var distance = Vector2.Distance(NPC.Position, NPC.Target.Position);

                if (distance < DetectionRadius)
                {
                    Operation = Operation.Chase;
                }
                else
                {
                    var displacement = TargetLastPosition - NPC.Position;
                    if (displacement.Length() > 1)
                    {
                        var look = Vector2.Normalize(displacement);
                        NPC.Position += Speed*DeltaSeconds*look;
                    }

                    SearchingTimeout -= DeltaSeconds;
                    if (SearchingTimeout <= 0) Operation = Operation.StopSearching;
                }
            };
        }

        public override void LoadContent()
        {
            Font = Game.Content.Load<SpriteFont>("hudFont");
        }

        public override void Update(float deltaSeconds)
        {
            DeltaSeconds = deltaSeconds;
            Operation = Operation.None;

            Rules[State][Operation.Any]();
            if (Operation != Operation.None) Rules[State][Operation]();
        }

        public override void Draw(float deltaSeconds)
        {
            var info = new List<String>();

            info.Add(string.Format("NPC state: {0}", State));

            if (State == State.Searching)
            {
                info.Add(string.Format("Searching timeout: {0:0.00}", SearchingTimeout));
                info.Add(string.Format("Target last position: {0:0.0}, {1:0.0}", TargetLastPosition.X, TargetLastPosition.Y));
            }

            var pos = new Vector2(10, 10);
            foreach (var str in info)
            {
                Game.SpriteBatch.DrawString(Font, str, pos, Color.Black);
                pos += new Vector2(0, 30);
            }
        }
    }
}
