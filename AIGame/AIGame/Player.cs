﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AIGame
{
    public class Player : Entity
    {
        private const int Width = 48;
        private const int Height = 48;
        private Vector2 TopLeftOffset { get; set; }

        public Player()
        {
            TopLeftOffset = new Vector2(Width / 2, Height / 2);
        }

        public override void Initialize()
        {
            var viewport = Game.Graphics.GraphicsDevice.Viewport;
            Position = new Vector2((float)viewport.Width / 2, (float)viewport.Height / 2);
        }

        public override void LoadContent()
        {
            Texture = new Texture2D(Game.GraphicsDevice, 1, 1);
            Texture.SetData(new[] { Color.White });
        }

        public override void Update(float deltaSeconds)
        {
            Move(deltaSeconds);

            ClampPositionToViewport();
        }

        public override void Draw(float deltaSeconds)
        {
            var topLeft = Position - TopLeftOffset;
            var rect = new Rectangle((int)topLeft.X, (int)topLeft.Y, Width, Height);

            Game.SpriteBatch.Draw(Texture, rect, Color.White);
        }

        private void Move(float deltaSeconds)
        {
            var keyState = Keyboard.GetState();

            // check to see if the user wants to move the player. we'll create a vector
            // called movement, which will store the sum of all the user's inputs.
            var movement = Vector2.Zero;

            if (keyState.IsKeyDown(Keys.Left))
            {
                movement.X -= 1;
            }
            if (keyState.IsKeyDown(Keys.Right))
            {
                movement.X += 1;
            }
            if (keyState.IsKeyDown(Keys.Up))
            {
                movement.Y -= 1;
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                movement.Y += 1;
            }

            const float speed = 200f;

            Position += movement * speed * deltaSeconds;
        }
    }
}
